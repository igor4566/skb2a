import express from 'express';
import cors from 'cors';

const app = express();
app.use(cors());
app.get('/', (req, res) => {
  res.json({
    hello: 'JS World',
  });
});
function yourName(name) {
  const fName = name.replace(/ * /g, ' ').replace(/^ */g, '').replace(/'/, '').match(/[a-zа-ёÇ-Ý]*/gi);
  const numbName = name.match(/[!-&(-@[-`]/g);
  let fullname;
  console.log(fName);
  if (numbName === null) {
    if (fName.length > 4 && fName.length < 7) {
      fullname = `${fName[4][0].toUpperCase()}${fName[4].slice(1).toLowerCase()} ${fName[0][0].toUpperCase()}. ${fName[2][0].toUpperCase()}.`;
    } else if (fName.length > 2 && fName.length < 5) {
      fullname = `${fName[2][0].toUpperCase()}${fName[2].slice(1).toLowerCase()} ${fName[0][0].toUpperCase()}.`;
    } else if (fName.length > 1 && fName.length < 3) {
      fullname = `${fName[0][0].toUpperCase()}${fName[0].slice(1).toLowerCase()}`;
    } else {
      fullname = 'Invalid fullname';
    }
  } else {
    fullname = 'Invalid fullname';
  }
  return fullname;
}
app.get('/task', (req, res) => {
  const name = req.query.fullname;
  const fName = yourName(name);
  res.send(fName.toString());
});

app.listen(3000, () => {
  console.log('Your app listening on port 3000!');
});
